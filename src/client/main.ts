import {bootstrap} from 'angular2/platform/browser';
import {enableProdMode} from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';
import {environment} from './app/environment';
import {FindParkingGoteborgApp} from './app/components/find-parking-goteborg.component';
import {FIREBASE_PROVIDERS, defaultFirebase, AngularFire} from 'angularfire2/angularfire2'; 
import 'rxjs/Rx';
import {MenyGoteborgApp} from './app/components/meny-goteborg.component';
import {RouteConfig, ROUTER_DIRECTIVES,Router, ROUTER_PROVIDERS} from 'angular2/router';

if (environment.production) {
  enableProdMode();
}
/*
bootstrap(FindParkingGoteborgApp,  [
  FIREBASE_PROVIDERS, 
  defaultFirebase('https://findparking.firebaseio.com/'),
  HTTP_PROVIDERS
]);
*/

bootstrap(MenyGoteborgApp,  [ROUTER_PROVIDERS,
  FIREBASE_PROVIDERS, 
  defaultFirebase('https://findparking.firebaseio.com/'),
  HTTP_PROVIDERS
]);

//MenyGoteborgApp
