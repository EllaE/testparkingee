import {Component, OnInit, Inject} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {AngularFire, FirebaseListObservable, FirebaseRef} from 'angularfire2';
import { Destination } from '../supporting-files/destination';
import { Trip } from '../supporting-files/trip';
import { Comment } from '../supporting-files/comment';
import { Parking } from '../supporting-files/parking';
import { ApiService } from '../services/api.service';
import { User } from '../supporting-files/user';
import { ParkingService } from '../services/parking.service';
import { OnActivate, Router } from 'angular2/router';
import { AccountService } from '../services/account.service';

@Component({
  moduleId: __moduleName,
  selector: 'parking-history',
  providers: [ApiService, ParkingService ],
  templateUrl: 'parking-history.component.html',
  styleUrls: ['parking-history.component.css'],
  directives: [], //array of components and directives that this template requires 
  pipes: []
})

//this page should show history, either users or from local storage
export class ParkingHistory {
    constructor(private _loginServ: AccountService){}
    
    
    ngOnInit() {
       // this.router.navigate(['/find-parking']);
      this._loginServ.getLocalUser();
    
    }
}
    