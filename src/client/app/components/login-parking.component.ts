import {Component, OnInit, Inject} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {AngularFire, FirebaseListObservable, FirebaseRef} from 'angularfire2';
import { Destination } from '../supporting-files/destination';
import { Trip } from '../supporting-files/trip';
import { Comment } from '../supporting-files/comment';
import { Parking } from '../supporting-files/parking';
import { ApiService } from '../services/api.service';
import { User } from '../supporting-files/user';
import { ParkingService } from '../services/parking.service';
import { OnActivate, Router } from 'angular2/router';

import { AccountService } from '../services/account.service';

@Component({
  moduleId: __moduleName,
  selector: 'login-parking',
  providers: [],
  templateUrl: 'login-parking.component.html',
  styleUrls: ['login-parking.component.css'],
  directives: [], //array of components and directives that this template requires 
  pipes: []
})

//this page should show history, either users or from local storage
export class LoginComponent {    
   // user: string;
    userAccount: User;
    logInData: User;
    constructor(@Inject(AccountService)private _loginServ: AccountService){
        
    }
    
    
  ngOnInit () {
    this.logInData = new User();
  }
  
  
      /**
     * Removes input from input fields username and password in user creation/log in form.
     */
    cleanAccountForm(){
      this.displayAccountFormFeedback(' ');
      (<HTMLInputElement>document.getElementById('usernameL')).value = '';
      (<HTMLInputElement>document.getElementById('passwordL')).value = '';
    }
    
        /**
     * Takes string text to be shown as feedback when user interacts with user creation/login
     * form. Displays text on that form.
     */
    displayAccountFormFeedback(feedbackText: string){
      let feedbackSpan = (<HTMLInputElement>document.getElementById('feedback-account-formL'));
      feedbackSpan.innerText = '';
      feedbackSpan.innerText = feedbackText;
    }
    
        /**
     * CreateOrLogIn is called when account form submit button is clicked. Depending on value on
     * that button (inner text) this function will either start function to log in user or to 
     * create new account.
     */
    createOrLogIn(){
      if((<HTMLInputElement>document.getElementById('confirm-btn-accountL')).innerText === 'Login'){
          if(this.logInData && this.logInData.name && this.logInData.logInPassword){
             this._loginServ.logInAccount(this.logInData);
              //NEED PROMISE 
          } else {
              this.displayAccountFormFeedback('Fill all fields.');
          }
        } 
    }
    
  
  //methods to controll form fields and give feedback
  //methods that act with buttons from form
  
  //temporar register
  registerClicked(){
     if((<HTMLInputElement>document.getElementById('register-btn-accountL')).innerText === 'Register'){
          if(this.logInData && this.logInData.name && this.logInData.logInPassword){
             this._loginServ.createAccount(this.logInData);
              //NEED PROMISE 
          } else {
              this.displayAccountFormFeedback('Fill all fields.');
          }
        }   
  }
  
  
}
   