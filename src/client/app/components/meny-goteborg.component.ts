import {Component, OnInit, Inject} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES,Router,  ROUTER_PROVIDERS} from 'angular2/router';
import {AngularFire, FirebaseListObservable, FirebaseRef} from 'angularfire2';
import { Destination } from '../supporting-files/destination';
import { Trip } from '../supporting-files/trip';
import { Comment } from '../supporting-files/comment';
import { Parking } from '../supporting-files/parking';
import { ApiService } from '../services/api.service';
import { User } from '../supporting-files/user';
import { ParkingService } from '../services/parking.service';
import {bootstrap} from 'angular2/platform/browser';

import {ParkingHistory} from '../components/parking-history.component';
import {FindParkingGoteborgApp} from '../components/find-parking-goteborg.component';
import { DROPDOWN_DIRECTIVES } from 'ng2-bootstrap/components/dropdown';
import {ROUTER_BINDINGS, ROUTER_PRIMARY_COMPONENT, RouterOutlet, RouterLink, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {LoginComponent} from '../components/login-parking.component';
import { AccountService } from '../services/account.service';


import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {FormBuilder, Validators} from 'angular2/common';

@Component({
  moduleId: __moduleName,
  selector: 'meny-goteborg-app',
  providers: [ApiService, ParkingService, AccountService,ROUTER_PROVIDERS],
  templateUrl: 'meny-goteborg.component.html',
  styleUrls: ['meny-goteborg.component.css'],
  directives: [ROUTER_DIRECTIVES, RouterOutlet, RouterLink, ParkingHistory, FindParkingGoteborgApp, DROPDOWN_DIRECTIVES ], //array of components and directives that this template requires 
  pipes: []
})

//Dropdown, DropdownMenu, DropdownToggle
@RouteConfig([
   { path: '/find-parking', component: FindParkingGoteborgApp, name: 'Home' },
   { path: '/history', component: ParkingHistory, name: 'History' },
   { path: '/login', component: LoginComponent, name: 'Login' },
   { path: '/**', redirectTo: ['Home'] }

])



export class MenyGoteborgApp {
  
    //observable test
    userAccountO:Observable<User>;
    tripsO:Observable<Trip[]>;
    userO:Observable<string>;
   
    //end
  
    userA: User;
    showUser: boolean = false;;
    showLogin: boolean = true;
    constructor(private router: Router, @Inject(AccountService) private loginServ: AccountService) {}

    ngOnInit() {
      
      //observable
      this.tripsO = this.loginServ.trips$;
      //this.loginServ.loadLocalUserO; ?
      //end
      
       // this.router.navigate(['/find-parking']);
      this.loginServ.getLocalUser();
      this.showUser = false;
      this.showLogin = true;
      this.showTheUser();
    
    
    }
    
    showTheUser() {
      if(this.loginServ.getUserAccount()){
        let user = this.loginServ.getUserAccount;
        //this.userA = user;
        this.showUser = true;
        this.showLogin = false;
      } else {
        this.showUser = false;
        this.showLogin = true;
      }
      
    }

}




