import {Component, OnInit, Inject} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {AngularFire, FirebaseListObservable, FirebaseRef} from 'angularfire2';
import { Destination } from '../supporting-files/destination';
import { Trip } from '../supporting-files/trip';
import { Comment } from '../supporting-files/comment';
import { Parking } from '../supporting-files/parking';
import { ApiService } from '../services/api.service';
import { User } from '../supporting-files/user';
import { ParkingService } from '../services/parking.service';

import { OnActivate, Router } from 'angular2/router';

import { AccountService } from '../services/account.service';


@Component({
  moduleId: __moduleName,
  selector: 'find-parking-goteborg-app',
  providers: [ApiService, ParkingService],
  templateUrl: 'find-parking-goteborg.component.html',
  styleUrls: ['find-parking-goteborg.component.css'],
  directives: [],
  pipes: []
})

export class FindParkingGoteborgApp {
  minNumberOfParkings: number = 3;
  user: string;
  userAccount: User;
  trips: Trip[] = [];
  newTrip: Trip = new Trip();
  constructor(@Inject(FirebaseRef) private _ref: Firebase, private apiService: ApiService, private parkingService: ParkingService, private _loginServ: AccountService) {}
  

  


  /**
   * showTrip displays given trip for editing purposes.
   */
  showTrip (trip: Trip) {
    this._loginServ.getDestinations(trip);
    //need promise this.newTrip = ...
  }
  
  /**
   * Creates new destination if previously added one has filled adress field.
   */
  newDestination () {
    var length = this.newTrip.destinations.length;
    
    if(this.newTrip.destinations){
       if(this.newTrip.destinations.length==0){
           this.newTrip.destinations.push(new Destination());
       }else {
           if(this.newTrip.destinations[length-1].address){
               this.newTrip.destinations.push(new Destination());
           }
       }
       this.validateFormFields();
    }
  }
  
  /**
   * Creates new commentar for given destination object.
   */
  newComment(destination: Destination) {
    var length = destination.comments.length;
    
    if(destination.comments){
       if(destination.comments.length==0){
           destination.comments.push(new Comment());
       }else {
           if(destination.comments[length-1].text){
               destination.comments.push(new Comment());
           }
       }
        this.validateFormFields(); 
    }
  }
  
  ngOnInit () {
    
  /*  
this._loginServ.userAccount.subscribe(updatedTodos => {
  
});
*/
    
    
    this.getLocal();
    this.user = this._loginServ.getLocalUser();
    this.user = this._loginServ.getLocalUser();
    this.trips = this._loginServ.getTrips();
    console.log('Test: '+ this.user + ' ' + this.trips.length);
    
  }
  
  /**
   * When table row representing specific trip is clicked then this function shows 
   * trip for editing purposes.
   */
  tripClicked (trip: Trip) {
    this.newTrip = trip;
    this._loginServ.getDestinations(trip);
  }
     
  /** 
   * This method takes the values from the input fields, 
   * creates a trip object and a destination object and adds 
   * them to the database.
   */
  add () {
    let validated = this.validateFormFields();
    if(validated === false){
      return;
    }
    
    let tripCreated = this._ref.child('/trips').push({
      name: this.newTrip.name,
      description: (this.newTrip.description) ? this.newTrip.description : null,
      userid: this.user,
      destinations: this.newTrip.destinations.length,
      createdat: Firebase.ServerValue.TIMESTAMP
    });
    
    tripCreated.then(() => {
      for (let destination of this.newTrip.destinations) {
        this.validateDestination(destination);
        let destinationCreated = this._ref.child('/destinations').push(
      { 
        address: destination.address, 
        time: destination.time, 
        tripid: tripCreated.key(),
        createdat: Firebase.ServerValue.TIMESTAMP
      });
      destinationCreated.then(() => {
        for (let comment of destination.comments) {
          this._ref.child('/comments').push(
            {
              text: comment.text,
              destinationid: destinationCreated.key(),
              createdat: Firebase.ServerValue.TIMESTAMP
            });
          }
        });
        
      }
      
      this.newTrip = new Trip();
      
    });
  }
  

  findParking(destination:Destination){
      if(destination.address){
        var beforeAdress = 'https://maps.googleapis.com/maps/api/geocode/json?address='
        var afterAdress = '&key=AIzaSyCoO_vczIXw8qrrNgaLzZNqNus5YFU9GnQ'
        var address = destination.address;
        
        if(destination.address.length > 7){
          if(destination.address.slice(-8)!="Göteborg"){
              address += '+Göteborg'
          }
        }else if(destination.address.length <= 7){
          address += '+Göteborg'
        } 
 
        var object = this.apiService.getFromApi(beforeAdress + address + afterAdress).then((result) => {
          let geocoding = result.results[0].geometry.location
          this.getParking(geocoding, destination, 100);
        });
      }
  }
  
  

   
 


    /**
     * Function removeLastDestination removes last destination from list of destinations
     * to newly created trip object. If trip has no destinations, none are removed.
     */
    removeLastDestination(){
      if(this.newTrip.destinations.length > 0){
        this.newTrip.destinations.pop();
      }      
    }
    
    /**
     * Function removeLastComment takes destination as argument and removes last Comment
     * object that is assigned to its variable of comments if any.
     */
    removeLastComment(destination: Destination){
      if(destination && destination.comments.length >0){
        destination.comments.pop();  
      }
    }
      
 
    /**
     * Function goes through object of Destination class and checks if it has any valid values
     * on some of its variables. Some get default value if they have none for the moment.
     */
    validateDestination(destination: Destination){  
        if(!destination.address){
           destination.address = 'none';
        }
        if(!destination.comments){
        }
        if(!destination.time){
          destination.time = '0';  
        }  
    }
    
  
    /**
     * Checks if trip object has name assigned to it, if not then it assigns name to it.
     */
    validateTripName(trip: Trip){
      if(!trip.name){
        trip.name = 'No name';
      }
    }
    
    /**
     * Function validateFormFields goes through form fields with id 'name' or class 'adress-field'
     * and checks if they have input value. If any of these fields is empty then this function returns
     * false aswell as it marks fields with class representing that they have invalid input. If field
     * became valid and has invalid class then its removed.
     */
    validateFormFields(): boolean{
      let isValid = true;
      let tripElement = (<HTMLInputElement>document.getElementById('name'));
      let tripName = tripElement.value; 
      let adressElements = <HTMLCollection>document.getElementsByClassName('adress-field');
      let commentElements = <HTMLCollection>document.getElementsByClassName('comment-field');
      
      //check if name is valid
      if(!tripName || tripName.length >30){
        tripElement.className += ' field-input-invalid'; //mark element as invalid
        isValid = false;
      } else {  //remove red class if any 
          tripElement.classList.remove('field-input-invalid');
      }
      
      //check if destination adresses are valid
      if(adressElements){
        for(let i = 0; i < adressElements.length; i++){
          if(!(<HTMLInputElement>adressElements[i]).value){
            //mark element with invalid class ng-invalid
            (<HTMLInputElement>adressElements[i]).classList.add('field-input-invalid'); 
            isValid = false;
          } else {
            (<HTMLInputElement>adressElements[i]).classList.remove('field-input-invalid'); 
            //check if class is ng-invalid, remove it then
          }
        }   
      }
      
      //check if destination comments are valid
      if(commentElements){
        for(let i = 0; i < commentElements.length; i++){
          if(!(<HTMLInputElement>commentElements[i]).value){
            //mark element with invalid class ng-invalid
            (<HTMLInputElement>commentElements[i]).classList.add('field-input-invalid'); 
            isValid = false;
          } else {
            (<HTMLInputElement>commentElements[i]).classList.remove('field-input-invalid'); 
            //check if class is ng-invalid, remove it then
          }
        }   
      }   
         
      return isValid;
    }
    
    startNewTrip(){
      this.newTrip = new Trip();
    }

    /*Account management */
    


    
 

   
    
 
  
    

    
    getParking(position, destination: Destination, radius) {
      let parkings: Parking[] = [];
      this.parkingService.publicTimeParkings(position, destination, radius, parkings).then((freeList) => {
        this.parkingService.publicTollParkings(position, destination, radius, freeList).then((completeList) => {
          if (completeList.length < this.minNumberOfParkings) {
            this.getParking(position, destination, radius+50);
          } else {
            completeList.sort((a, b) => {
              return a.distance - b.distance;
            });
            destination.parking = completeList; 
          }
        });
      });
    }
    
    
    
    // TEST
    
    getLocal() {
      this._loginServ.getLocalUserP().then(userKey => {
        this.user = userKey;
        this.user = this._loginServ.getLocalUser();
      this.trips = this._loginServ.getTrips();
       console.log('Test: '+ this.user + ' ' + this.trips.length);
        console.log(this.user + 'bbb');
        
      });
      
    }
    
}
