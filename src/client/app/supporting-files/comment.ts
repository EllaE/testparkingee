/**
 * Class Comment is used to save information about comment made for a specific
 * destination and timestamp when that comment was created.
 */
export class Comment {
  text: string;
  timestamp: any;
}