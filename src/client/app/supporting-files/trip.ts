import { Destination } from './destination';


/**
 * Class Trip represents trip that user has planned along with its description,
 * destination (parking) adresses with comments and time for every destination.
 */
export class Trip {
    id:string;
    name:string;
    description: string;
    destinations: Destination[];
    constructor(){
        this.destinations  = [];
    }
    
}