/**
 * Class Parking represents one parking with associated information about its
 * cost, time allowed to park, name and distance.
 * 
 */
export class Parking {
  private name: string;
  distance: number;
  private cost: string;
  private maxTime: string;
  
  constructor(name: string, distance: number, cost: string, maxTime: string) {
    this.name = name;
    this.distance = distance;
    this.cost = cost;
    this.maxTime = maxTime;
  }
}