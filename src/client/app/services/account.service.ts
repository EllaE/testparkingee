import { Http, Response, Headers } from 'angular2/http';
import { Injectable } from 'angular2/core';
import { Observable } from 'rxjs/Observable';
import { Destination } from '../supporting-files/destination';
import { Trip } from '../supporting-files/trip';
import { Comment } from '../supporting-files/comment';
import { Parking } from '../supporting-files/parking';
import { ApiService } from '../services/api.service';
import { User } from '../supporting-files/user';
import { ParkingService } from '../services/parking.service';
import { OnActivate, Router } from 'angular2/router';
import {AngularFire, FirebaseListObservable, FirebaseRef} from 'angularfire2';
import {Component, OnInit, Inject} from 'angular2/core';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
/**
 * A service to easily connect to and get user data from firebase
 */
@Injectable()
export class AccountService {
  private user: string;
  private userAccount: User = null; //empty when this is instantiated first time
  trips: Trip[] = []; //should be private? 
  //newTrip: Trip = new Trip(); //shouldnt be here
  
  //test observables
  userAccount$:Observable<User>;
  trips$:Observable<Trip[]>;
  user$:Observable<string>;
  
  private _tripsObserver: Observer<Trip[]>;
  private _userObserver: Observer<string>;
  private _accountObserver: Observer<User>;
  private _dataStore: {
        tripsT: Trip[],
        userK: string,
        userA: User
    };

  constructor(@Inject(FirebaseRef) private _ref: Firebase) {
      //get user local data
      console.log('account service created');
      
      this.trips$ = new Observable(observer => this._tripsObserver = observer).share();
      this.user$ = new Observable(observer => this._userObserver = observer).share();
      this.userAccount$ = new Observable(observer => this._accountObserver = observer).share();
      this._dataStore = {tripsT: [],
        userK: '',
        userA: null 
      }; 
      
        
   }
   
   //gets local storage user or creates new one if needed
   getLocalUser():string{
       if(!this.user){
           this.loadUser();
           this.loadUserTrips();
       }
       return this.user;
   }
   
      getLocalUserP():Promise<any>{
       if(!this.user){
           this.loadUser();
           this.loadUserTrips();
       }
       return Promise.resolve(this.user);
   }
   

   
   //use to log in user if user is not logged in and uses for to log in, provide data
   logInAccount(user:User):User{ //NEED this to return promise??
     
       this._ref.child('/userAccounts').orderByChild('name').equalTo(user.name).on('value', (userAccounts) => {
        
            userAccounts.forEach((aUser) => {
                if(user.logInPassword === aUser.child('password').val()){
                user.setPassword(aUser.child('name').val());
                user.userKey = aUser.key(); //needed to load trips
                }
                
            }); 
         
            if(user.userKey){ //if any was loaded then it got key now to load trips with
                this.user = user.userKey;
                this.userAccount = user;
                this.loadUserTrips(); //load user trips 
              //  this.newTrip = new Trip(); //otherwise can be some weird probs when editing trips?
                //return user;
            } else {
                //return null;
            }      
       });
       if(this.userAccount){
           console.log('returning user');
           return user;
       } else {
           console.log('returning null');
           return null;
       }
   }
   
   
   createAccount(user:User){
        // this.checkIfUserExists(); check first if user exists.
        this._ref.child('/userAccounts').orderByChild('name').equalTo(user.name).once('value', (databaseUsers) => {
          if (!databaseUsers.exists()) {
            let userCreated = this._ref.child('/userAccounts').push({
              name: user.name,
              password: user.logInPassword
            });
    
            userCreated.then(() => {
              //log in user!
              this.logInAccount(user);
            });      
          } else {
           // this.displayAccountFormFeedback('Username already exist!');
          }
        });

   }
   
   
   
   getUserAccount():User{
       return this.userAccount; //may be null if user not logged in
   }
   
  
  /**  This method creates new user token for 
   * firebase and saves it in local storage
   */
  private createAndSaveUser(){
        localStorage.clear();
        let userCreated = this._ref.child('/users').push ({
          timestamp: Firebase.ServerValue.TIMESTAMP  
        });
       
        userCreated.then((data) => {
           this.user = userCreated.key();
           localStorage.setItem('USER_DB', this.user);
           //this.getTrip ();
           console.log('created user with name: '+ this.user);
        });
    }
   
  /** This method loads user token from local storage 
   * if any existent, otherwise it runs createAndSaveUser 
   */
  private loadUser(){
        if(localStorage.getItem('USER_DB')) {
            this.user = localStorage.getItem('USER_DB');
            this._ref.child('/users/' + this.user).once('value', (result) => {
              if (result.exists()) {
                this.loadUserTrips(); 
                console.log('load user with name: '+ this.user);
              } else {
                this.createAndSaveUser();
              }
            });         
        } else {
          this.createAndSaveUser();
        } 
        
  }

  //rest
  
  logOutUser(){
      //1. remove all user reference, check for loclalStorage if any
      this.user = null;
      this.userAccount = null;
      this.trips = [];
    //  this.newTrip = new Trip();
      this.loadUser(); //load local storage user if any, create new otherwise
    }
     
     
 /**
   * getTrip fetches all trip data for specific user from firebase.
   */
 private loadUserTrips () {
    this._ref.child('/trips').orderByChild('userid').equalTo(this.user).on('value', (userTrips) => {
      this.trips = [];
      userTrips.forEach((trip) => {
          let t: Trip = new Trip();
          t.name = trip.child("name").val();
          t.description = trip.child('description').val();
          t.id = trip.key();
          this.trips.push(t);
      });
      this.trips.reverse();
      if(this.userAccount){
          this.userAccount.trips = this.trips;
          
      }
      
    });
    
  }
  
  getTrips(){
      if(!this.trips){
          this.loadUserTrips();
      }
                 
      return this.trips;
  }
  
  
   /**
   * Fetches destinations for given Trip object from firebase.
   */
  getDestinations (trip: Trip) {
    trip.destinations = [];
    this._ref.child('/destinations').orderByChild('tripid').equalTo(trip.id).on('value', (tripDestinations) => {
      tripDestinations.forEach((destination) => {
          let d: Destination = new Destination();
          d.id = destination.key();
          d.address = destination.child('address').val();
          d.time = destination.child('time').val();
          trip.destinations.push(d);
          this.getComments(d);
      });
    });
  }
  
  /**
   * Fetches comments for given destination object from firebase database.
   */
  getComments (destination: Destination) {
    destination.comments = [];
    this._ref.child('/comments').orderByChild('destinationid').equalTo(destination.id).on('value', (destinationComments) => {
      destinationComments.forEach((comment) => {
          let c: Comment = new Comment();
          c.text = comment.child('text').val();
          c.timestamp = comment.child('createdat').val();
          destination.comments.push(c);
      });
    });
  }

  
}

/*
1. when user starts app this service is to load local storage account: use getLocalUser method
2. when user goes to log in and choses log in use method to log in acc. 
3. this.trips is connected with localStorage when no user is logged in and with userAcc if loggedin


*/
