import {Inject, Injectable} from 'angular2/core';
import {AngularFire, FirebaseListObservable, FirebaseRef} from 'angularfire2';
import { Destination } from '../supporting-files/destination';
import { Parking } from '../supporting-files/parking';
import { CoordinateDistances } from '../supporting-files/coordinate-distances';

/**
 * A service for retrieving valid parkings
 */
@Injectable()
export class ParkingService {
  
  constructor(@Inject(FirebaseRef) private _ref: Firebase) { }
  
  /**
   * Retrieves all free time parkings within a give radius
   * return a promise 
   */
  publicTimeParkings(position, destination, radius, parkings) : Promise<Parking[]> {
      return new Promise((resolve) => {
        let bigBox = CoordinateDistances.square(position.lat, position.lng, radius);
        this._ref.child('/PublicTimeParkings').once("value", (snapshot) => {
          snapshot.forEach((obj) => {
            
            if (obj.val().Lat >= bigBox.latitude.min && obj.val().Lat <= bigBox.latitude.max && 
                  obj.val().Long >= bigBox.longitude.min && obj.val().Long <= bigBox.longitude.max) {
                let distance = CoordinateDistances.distanceBetweenPoints(position.lat, position.lng, obj.val().Lat, obj.val().Long);
                if (distance <= radius) {
                  let time: number  = parseInt(destination.time);
                  if (isNaN(time)) {
                    time = 0;
                  }
                  let maxTime: number = this.returnTimeInMins(obj.val().MaxParkingTime);
                  if (maxTime >= time) {
                    parkings.push(new Parking(obj.val().Name, Math.round(distance), 'Gratis', obj.val().MaxParkingTime));
                  }
                }
            }
          });
          resolve(parkings);
        });
      });
    }
    
    /**
     * Retrieves all toll time parkings within a give radius
     * return a promise
     */
    publicTollParkings(position, destination, radius, parkings) : Promise<Parking[]> {
      return new Promise((resolve) => {
        let bigBox = CoordinateDistances.square(position.lat, position.lng, radius);
        this._ref.child('/PublicTollParkings').once("value", (snapshot) => {
          snapshot.forEach((obj) => {
              
            if (obj.val().Lat >= bigBox.latitude.min && obj.val().Lat <= bigBox.latitude.max && 
                  obj.val().Long >= bigBox.longitude.min && obj.val().Long <= bigBox.longitude.max) {
              let distance = CoordinateDistances.distanceBetweenPoints(position.lat, position.lng, obj.val().Lat, obj.val().Long);
              if (distance <= radius) {
                  let time: number  = parseInt(destination.time);
                  if (isNaN(time)) {
                    time = 0;
                  }
                  let maxTime: number = this.returnTimeInMins(obj.val().MaxParkingTime);
                  if (maxTime >= time) {
                    parkings.push(new Parking(obj.val().Name, Math.round(distance), obj.val().ParkingCost, obj.val().MaxParkingTime));
                  }
              }
            }
          });
          resolve(parkings);
        });
      });
    }
    
    /**
     * Function returnTimeInMins takes string in format XX min and returns it as
     * number. If parameter given is invalid returns -1. 
     */
    returnTimeInMins(time: string): number{
      let timeNr;
      let splitted = time.split(' ');
      timeNr = parseInt(splitted[0]);
      if(typeof timeNr === 'number'){
        if (splitted.length > 1 && splitted[1] === 'tim') {
          timeNr *= 60;
        }
        return timeNr;
      } else {
        return -1;
      }  
    }
}