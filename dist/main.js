System.register(['angular2/platform/browser', 'angular2/core', 'angular2/http', './app/environment', 'angularfire2/angularfire2', 'rxjs/Rx', './app/components/meny-goteborg.component', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, core_1, http_1, environment_1, angularfire2_1, meny_goteborg_component_1, router_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (environment_1_1) {
                environment_1 = environment_1_1;
            },
            function (angularfire2_1_1) {
                angularfire2_1 = angularfire2_1_1;
            },
            function (_1) {},
            function (meny_goteborg_component_1_1) {
                meny_goteborg_component_1 = meny_goteborg_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            if (environment_1.environment.production) {
                core_1.enableProdMode();
            }
            /*
            bootstrap(FindParkingGoteborgApp,  [
              FIREBASE_PROVIDERS,
              defaultFirebase('https://findparking.firebaseio.com/'),
              HTTP_PROVIDERS
            ]);
            */
            browser_1.bootstrap(meny_goteborg_component_1.MenyGoteborgApp, [router_1.ROUTER_PROVIDERS,
                angularfire2_1.FIREBASE_PROVIDERS,
                angularfire2_1.defaultFirebase('https://findparking.firebaseio.com/'),
                http_1.HTTP_PROVIDERS
            ]);
        }
    }
});
//MenyGoteborgApp
//# sourceMappingURL=main.js.map