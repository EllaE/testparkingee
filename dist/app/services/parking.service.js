System.register(['angular2/core', 'angularfire2', '../supporting-files/parking', '../supporting-files/coordinate-distances'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, angularfire2_1, parking_1, coordinate_distances_1;
    var ParkingService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (angularfire2_1_1) {
                angularfire2_1 = angularfire2_1_1;
            },
            function (parking_1_1) {
                parking_1 = parking_1_1;
            },
            function (coordinate_distances_1_1) {
                coordinate_distances_1 = coordinate_distances_1_1;
            }],
        execute: function() {
            /**
             * A service for retrieving valid parkings
             */
            ParkingService = (function () {
                function ParkingService(_ref) {
                    this._ref = _ref;
                }
                /**
                 * Retrieves all free time parkings within a give radius
                 * return a promise
                 */
                ParkingService.prototype.publicTimeParkings = function (position, destination, radius, parkings) {
                    var _this = this;
                    return new Promise(function (resolve) {
                        var bigBox = coordinate_distances_1.CoordinateDistances.square(position.lat, position.lng, radius);
                        _this._ref.child('/PublicTimeParkings').once("value", function (snapshot) {
                            snapshot.forEach(function (obj) {
                                if (obj.val().Lat >= bigBox.latitude.min && obj.val().Lat <= bigBox.latitude.max &&
                                    obj.val().Long >= bigBox.longitude.min && obj.val().Long <= bigBox.longitude.max) {
                                    var distance = coordinate_distances_1.CoordinateDistances.distanceBetweenPoints(position.lat, position.lng, obj.val().Lat, obj.val().Long);
                                    if (distance <= radius) {
                                        var time = parseInt(destination.time);
                                        if (isNaN(time)) {
                                            time = 0;
                                        }
                                        var maxTime = _this.returnTimeInMins(obj.val().MaxParkingTime);
                                        if (maxTime >= time) {
                                            parkings.push(new parking_1.Parking(obj.val().Name, Math.round(distance), 'Gratis', obj.val().MaxParkingTime));
                                        }
                                    }
                                }
                            });
                            resolve(parkings);
                        });
                    });
                };
                /**
                 * Retrieves all toll time parkings within a give radius
                 * return a promise
                 */
                ParkingService.prototype.publicTollParkings = function (position, destination, radius, parkings) {
                    var _this = this;
                    return new Promise(function (resolve) {
                        var bigBox = coordinate_distances_1.CoordinateDistances.square(position.lat, position.lng, radius);
                        _this._ref.child('/PublicTollParkings').once("value", function (snapshot) {
                            snapshot.forEach(function (obj) {
                                if (obj.val().Lat >= bigBox.latitude.min && obj.val().Lat <= bigBox.latitude.max &&
                                    obj.val().Long >= bigBox.longitude.min && obj.val().Long <= bigBox.longitude.max) {
                                    var distance = coordinate_distances_1.CoordinateDistances.distanceBetweenPoints(position.lat, position.lng, obj.val().Lat, obj.val().Long);
                                    if (distance <= radius) {
                                        var time = parseInt(destination.time);
                                        if (isNaN(time)) {
                                            time = 0;
                                        }
                                        var maxTime = _this.returnTimeInMins(obj.val().MaxParkingTime);
                                        if (maxTime >= time) {
                                            parkings.push(new parking_1.Parking(obj.val().Name, Math.round(distance), obj.val().ParkingCost, obj.val().MaxParkingTime));
                                        }
                                    }
                                }
                            });
                            resolve(parkings);
                        });
                    });
                };
                /**
                 * Function returnTimeInMins takes string in format XX min and returns it as
                 * number. If parameter given is invalid returns -1.
                 */
                ParkingService.prototype.returnTimeInMins = function (time) {
                    var timeNr;
                    var splitted = time.split(' ');
                    timeNr = parseInt(splitted[0]);
                    if (typeof timeNr === 'number') {
                        if (splitted.length > 1 && splitted[1] === 'tim') {
                            timeNr *= 60;
                        }
                        return timeNr;
                    }
                    else {
                        return -1;
                    }
                };
                ParkingService = __decorate([
                    core_1.Injectable(),
                    __param(0, core_1.Inject(angularfire2_1.FirebaseRef)), 
                    __metadata('design:paramtypes', [Firebase])
                ], ParkingService);
                return ParkingService;
            }());
            exports_1("ParkingService", ParkingService);
        }
    }
});
//# sourceMappingURL=parking.service.js.map