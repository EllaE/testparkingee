System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Parking;
    return {
        setters:[],
        execute: function() {
            /**
             * Class Parking represents one parking with associated information about its
             * cost, time allowed to park, name and distance.
             *
             */
            Parking = (function () {
                function Parking(name, distance, cost, maxTime) {
                    this.name = name;
                    this.distance = distance;
                    this.cost = cost;
                    this.maxTime = maxTime;
                }
                return Parking;
            }());
            exports_1("Parking", Parking);
        }
    }
});
//# sourceMappingURL=parking.js.map