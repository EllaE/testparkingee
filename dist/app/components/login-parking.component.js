System.register(['angular2/core', '../supporting-files/user', '../services/account.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, user_1, account_service_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_1_1) {
                user_1 = user_1_1;
            },
            function (account_service_1_1) {
                account_service_1 = account_service_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent(_loginServ) {
                    this._loginServ = _loginServ;
                }
                LoginComponent.prototype.ngOnInit = function () {
                    this.logInData = new user_1.User();
                };
                /**
               * Removes input from input fields username and password in user creation/log in form.
               */
                LoginComponent.prototype.cleanAccountForm = function () {
                    this.displayAccountFormFeedback(' ');
                    document.getElementById('usernameL').value = '';
                    document.getElementById('passwordL').value = '';
                };
                /**
             * Takes string text to be shown as feedback when user interacts with user creation/login
             * form. Displays text on that form.
             */
                LoginComponent.prototype.displayAccountFormFeedback = function (feedbackText) {
                    var feedbackSpan = document.getElementById('feedback-account-formL');
                    feedbackSpan.innerText = '';
                    feedbackSpan.innerText = feedbackText;
                };
                /**
             * CreateOrLogIn is called when account form submit button is clicked. Depending on value on
             * that button (inner text) this function will either start function to log in user or to
             * create new account.
             */
                LoginComponent.prototype.createOrLogIn = function () {
                    if (document.getElementById('confirm-btn-accountL').innerText === 'Login') {
                        if (this.logInData && this.logInData.name && this.logInData.logInPassword) {
                            this._loginServ.logInAccount(this.logInData);
                        }
                        else {
                            this.displayAccountFormFeedback('Fill all fields.');
                        }
                    }
                };
                //methods to controll form fields and give feedback
                //methods that act with buttons from form
                //temporar register
                LoginComponent.prototype.registerClicked = function () {
                    if (document.getElementById('register-btn-accountL').innerText === 'Register') {
                        if (this.logInData && this.logInData.name && this.logInData.logInPassword) {
                            this._loginServ.createAccount(this.logInData);
                        }
                        else {
                            this.displayAccountFormFeedback('Fill all fields.');
                        }
                    }
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'login-parking',
                        providers: [],
                        templateUrl: 'login-parking.component.html',
                        styleUrls: ['login-parking.component.css'],
                        directives: [],
                        pipes: []
                    }),
                    __param(0, core_1.Inject(account_service_1.AccountService)), 
                    __metadata('design:paramtypes', [account_service_1.AccountService])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
//# sourceMappingURL=login-parking.component.js.map