System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var User;
    return {
        setters:[],
        execute: function() {
            /**
             * Class User is used to handle user accounts in application.
             */
            User = (function () {
                function User() {
                }
                /**
                 * function changePassword takes in new password and old password as parameters
                 * and if the oldpassword matches users password saved in _password then new
                 * password is assigned as users password. Returns true if password changed.
                 */
                User.prototype.changePassword = function (newPassword, oldPassword) {
                    if (oldPassword === this._password && newPassword.length >= 8) {
                        this._password = newPassword;
                        return true;
                    }
                    else {
                        return false;
                    }
                };
                /**
                 * Function setPassword is to be used only when user account is created for
                 * the first time as it assigns users protected password. This function should
                 * be removed and setting password should be instead put in constructor of class.
                 */
                User.prototype.setPassword = function (firstTimePassword) {
                    if (!this._password) {
                        this._password = firstTimePassword;
                    }
                };
                /**
                 * Function addTrip can be used to add trip to users assigned trips.
                 * Unused at the moment.
                 */
                User.prototype.addTrip = function (trip) {
                    this.trips.push(trip);
                };
                /**
                 * Function validatePassword takes given password parameter and compares
                 * it against users protected password. It returns true if passwords match.
                 */
                User.prototype.validatePassword = function (passwordToCompare) {
                    if (passwordToCompare === this._password) {
                        return true;
                    }
                    else {
                        return false;
                    }
                };
                /**
                 * Function isUserValid checks password integrity for user account.
                 */
                User.prototype.isUserValid = function () {
                    if (this._password === this.logInPassword) {
                        return true;
                    }
                    else {
                        return false;
                    }
                };
                return User;
            }());
            exports_1("User", User);
        }
    }
});
//# sourceMappingURL=user.js.map