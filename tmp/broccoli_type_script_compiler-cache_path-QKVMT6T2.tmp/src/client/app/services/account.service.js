System.register(['angular2/core', 'rxjs/Observable', '../supporting-files/destination', '../supporting-files/trip', '../supporting-files/comment', 'angularfire2', 'rxjs/add/operator/share', 'rxjs/add/operator/map'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, Observable_1, destination_1, trip_1, comment_1, angularfire2_1, core_2;
    var AccountService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
                core_2 = core_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (destination_1_1) {
                destination_1 = destination_1_1;
            },
            function (trip_1_1) {
                trip_1 = trip_1_1;
            },
            function (comment_1_1) {
                comment_1 = comment_1_1;
            },
            function (angularfire2_1_1) {
                angularfire2_1 = angularfire2_1_1;
            },
            function (_1) {},
            function (_2) {}],
        execute: function() {
            /**
             * A service to easily connect to and get user data from firebase
             */
            AccountService = (function () {
                function AccountService(_ref) {
                    var _this = this;
                    this._ref = _ref;
                    this.userAccount = null; //empty when this is instantiated first time
                    this.trips = []; //should be private? 
                    //get user local data
                    console.log('account service created');
                    this.trips$ = new Observable_1.Observable(function (observer) { return _this._tripsObserver = observer; }).share();
                    this.user$ = new Observable_1.Observable(function (observer) { return _this._userObserver = observer; }).share();
                    this.userAccount$ = new Observable_1.Observable(function (observer) { return _this._accountObserver = observer; }).share();
                    this._dataStore = { tripsT: [],
                        userK: '',
                        userA: null
                    };
                }
                //gets local storage user or creates new one if needed
                AccountService.prototype.getLocalUser = function () {
                    if (!this.user) {
                        this.loadUser();
                        this.loadUserTrips();
                    }
                    return this.user;
                };
                AccountService.prototype.getLocalUserP = function () {
                    if (!this.user) {
                        this.loadUser();
                        this.loadUserTrips();
                    }
                    return Promise.resolve(this.user);
                };
                //use to log in user if user is not logged in and uses for to log in, provide data
                AccountService.prototype.logInAccount = function (user) {
                    var _this = this;
                    this._ref.child('/userAccounts').orderByChild('name').equalTo(user.name).on('value', function (userAccounts) {
                        userAccounts.forEach(function (aUser) {
                            if (user.logInPassword === aUser.child('password').val()) {
                                user.setPassword(aUser.child('name').val());
                                user.userKey = aUser.key(); //needed to load trips
                            }
                        });
                        if (user.userKey) {
                            _this.user = user.userKey;
                            _this.userAccount = user;
                            _this.loadUserTrips(); //load user trips 
                        }
                        else {
                        }
                    });
                    if (this.userAccount) {
                        console.log('returning user');
                        return user;
                    }
                    else {
                        console.log('returning null');
                        return null;
                    }
                };
                AccountService.prototype.createAccount = function (user) {
                    var _this = this;
                    // this.checkIfUserExists(); check first if user exists.
                    this._ref.child('/userAccounts').orderByChild('name').equalTo(user.name).once('value', function (databaseUsers) {
                        if (!databaseUsers.exists()) {
                            var userCreated = _this._ref.child('/userAccounts').push({
                                name: user.name,
                                password: user.logInPassword
                            });
                            userCreated.then(function () {
                                //log in user!
                                _this.logInAccount(user);
                            });
                        }
                        else {
                        }
                    });
                };
                AccountService.prototype.getUserAccount = function () {
                    return this.userAccount; //may be null if user not logged in
                };
                /**  This method creates new user token for
                 * firebase and saves it in local storage
                 */
                AccountService.prototype.createAndSaveUser = function () {
                    var _this = this;
                    localStorage.clear();
                    var userCreated = this._ref.child('/users').push({
                        timestamp: Firebase.ServerValue.TIMESTAMP
                    });
                    userCreated.then(function (data) {
                        _this.user = userCreated.key();
                        localStorage.setItem('USER_DB', _this.user);
                        //this.getTrip ();
                        console.log('created user with name: ' + _this.user);
                    });
                };
                /** This method loads user token from local storage
                 * if any existent, otherwise it runs createAndSaveUser
                 */
                AccountService.prototype.loadUser = function () {
                    var _this = this;
                    if (localStorage.getItem('USER_DB')) {
                        this.user = localStorage.getItem('USER_DB');
                        this._ref.child('/users/' + this.user).once('value', function (result) {
                            if (result.exists()) {
                                _this.loadUserTrips();
                                console.log('load user with name: ' + _this.user);
                            }
                            else {
                                _this.createAndSaveUser();
                            }
                        });
                    }
                    else {
                        this.createAndSaveUser();
                    }
                };
                //rest
                AccountService.prototype.logOutUser = function () {
                    //1. remove all user reference, check for loclalStorage if any
                    this.user = null;
                    this.userAccount = null;
                    this.trips = [];
                    //  this.newTrip = new Trip();
                    this.loadUser(); //load local storage user if any, create new otherwise
                };
                /**
                  * getTrip fetches all trip data for specific user from firebase.
                  */
                AccountService.prototype.loadUserTrips = function () {
                    var _this = this;
                    this._ref.child('/trips').orderByChild('userid').equalTo(this.user).on('value', function (userTrips) {
                        _this.trips = [];
                        userTrips.forEach(function (trip) {
                            var t = new trip_1.Trip();
                            t.name = trip.child("name").val();
                            t.description = trip.child('description').val();
                            t.id = trip.key();
                            _this.trips.push(t);
                        });
                        _this.trips.reverse();
                        if (_this.userAccount) {
                            _this.userAccount.trips = _this.trips;
                        }
                    });
                };
                AccountService.prototype.getTrips = function () {
                    if (!this.trips) {
                        this.loadUserTrips();
                    }
                    return this.trips;
                };
                /**
                * Fetches destinations for given Trip object from firebase.
                */
                AccountService.prototype.getDestinations = function (trip) {
                    var _this = this;
                    trip.destinations = [];
                    this._ref.child('/destinations').orderByChild('tripid').equalTo(trip.id).on('value', function (tripDestinations) {
                        tripDestinations.forEach(function (destination) {
                            var d = new destination_1.Destination();
                            d.id = destination.key();
                            d.address = destination.child('address').val();
                            d.time = destination.child('time').val();
                            trip.destinations.push(d);
                            _this.getComments(d);
                        });
                    });
                };
                /**
                 * Fetches comments for given destination object from firebase database.
                 */
                AccountService.prototype.getComments = function (destination) {
                    destination.comments = [];
                    this._ref.child('/comments').orderByChild('destinationid').equalTo(destination.id).on('value', function (destinationComments) {
                        destinationComments.forEach(function (comment) {
                            var c = new comment_1.Comment();
                            c.text = comment.child('text').val();
                            c.timestamp = comment.child('createdat').val();
                            destination.comments.push(c);
                        });
                    });
                };
                AccountService = __decorate([
                    core_1.Injectable(),
                    __param(0, core_2.Inject(angularfire2_1.FirebaseRef)), 
                    __metadata('design:paramtypes', [Firebase])
                ], AccountService);
                return AccountService;
            }());
            exports_1("AccountService", AccountService);
        }
    }
});
/*
1. when user starts app this service is to load local storage account: use getLocalUser method
2. when user goes to log in and choses log in use method to log in acc.
3. this.trips is connected with localStorage when no user is logged in and with userAcc if loggedin


*/
//# sourceMappingURL=account.service.js.map