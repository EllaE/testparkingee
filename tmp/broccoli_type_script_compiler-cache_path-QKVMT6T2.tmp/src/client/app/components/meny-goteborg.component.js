System.register(['angular2/core', 'angular2/router', '../services/api.service', '../services/parking.service', '../components/parking-history.component', '../components/find-parking-goteborg.component', 'ng2-bootstrap/components/dropdown', '../components/login-parking.component', '../services/account.service', 'rxjs/add/operator/share', 'rxjs/add/operator/map'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, router_1, api_service_1, parking_service_1, parking_history_component_1, find_parking_goteborg_component_1, dropdown_1, router_2, login_parking_component_1, account_service_1;
    var MenyGoteborgApp;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (parking_service_1_1) {
                parking_service_1 = parking_service_1_1;
            },
            function (parking_history_component_1_1) {
                parking_history_component_1 = parking_history_component_1_1;
            },
            function (find_parking_goteborg_component_1_1) {
                find_parking_goteborg_component_1 = find_parking_goteborg_component_1_1;
            },
            function (dropdown_1_1) {
                dropdown_1 = dropdown_1_1;
            },
            function (login_parking_component_1_1) {
                login_parking_component_1 = login_parking_component_1_1;
            },
            function (account_service_1_1) {
                account_service_1 = account_service_1_1;
            },
            function (_1) {},
            function (_2) {}],
        execute: function() {
            MenyGoteborgApp = (function () {
                function MenyGoteborgApp(router, loginServ) {
                    this.router = router;
                    this.loginServ = loginServ;
                    this.showUser = false;
                    this.showLogin = true;
                }
                ;
                MenyGoteborgApp.prototype.ngOnInit = function () {
                    //observable
                    this.tripsO = this.loginServ.trips$;
                    //this.loginServ.loadLocalUserO; ?
                    //end
                    // this.router.navigate(['/find-parking']);
                    this.loginServ.getLocalUser();
                    this.showUser = false;
                    this.showLogin = true;
                    this.showTheUser();
                };
                MenyGoteborgApp.prototype.showTheUser = function () {
                    if (this.loginServ.getUserAccount()) {
                        var user = this.loginServ.getUserAccount;
                        //this.userA = user;
                        this.showUser = true;
                        this.showLogin = false;
                    }
                    else {
                        this.showUser = false;
                        this.showLogin = true;
                    }
                };
                MenyGoteborgApp = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'meny-goteborg-app',
                        providers: [api_service_1.ApiService, parking_service_1.ParkingService, account_service_1.AccountService, router_1.ROUTER_PROVIDERS],
                        templateUrl: 'meny-goteborg.component.html',
                        styleUrls: ['meny-goteborg.component.css'],
                        directives: [router_1.ROUTER_DIRECTIVES, router_2.RouterOutlet, router_2.RouterLink, parking_history_component_1.ParkingHistory, find_parking_goteborg_component_1.FindParkingGoteborgApp, dropdown_1.DROPDOWN_DIRECTIVES],
                        pipes: []
                    }),
                    router_1.RouteConfig([
                        { path: '/find-parking', component: find_parking_goteborg_component_1.FindParkingGoteborgApp, name: 'Home' },
                        { path: '/history', component: parking_history_component_1.ParkingHistory, name: 'History' },
                        { path: '/login', component: login_parking_component_1.LoginComponent, name: 'Login' },
                        { path: '/**', redirectTo: ['Home'] }
                    ]),
                    __param(1, core_1.Inject(account_service_1.AccountService)), 
                    __metadata('design:paramtypes', [router_1.Router, account_service_1.AccountService])
                ], MenyGoteborgApp);
                return MenyGoteborgApp;
            }());
            exports_1("MenyGoteborgApp", MenyGoteborgApp);
        }
    }
});
//# sourceMappingURL=meny-goteborg.component.js.map