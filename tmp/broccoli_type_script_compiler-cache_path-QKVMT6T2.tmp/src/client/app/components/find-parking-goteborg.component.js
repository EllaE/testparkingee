System.register(['angular2/core', 'angularfire2', '../supporting-files/destination', '../supporting-files/trip', '../supporting-files/comment', '../services/api.service', '../services/parking.service', '../services/account.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, angularfire2_1, destination_1, trip_1, comment_1, api_service_1, parking_service_1, account_service_1;
    var FindParkingGoteborgApp;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (angularfire2_1_1) {
                angularfire2_1 = angularfire2_1_1;
            },
            function (destination_1_1) {
                destination_1 = destination_1_1;
            },
            function (trip_1_1) {
                trip_1 = trip_1_1;
            },
            function (comment_1_1) {
                comment_1 = comment_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (parking_service_1_1) {
                parking_service_1 = parking_service_1_1;
            },
            function (account_service_1_1) {
                account_service_1 = account_service_1_1;
            }],
        execute: function() {
            FindParkingGoteborgApp = (function () {
                function FindParkingGoteborgApp(_ref, apiService, parkingService, _loginServ) {
                    this._ref = _ref;
                    this.apiService = apiService;
                    this.parkingService = parkingService;
                    this._loginServ = _loginServ;
                    this.minNumberOfParkings = 3;
                    this.trips = [];
                    this.newTrip = new trip_1.Trip();
                }
                /**
                 * showTrip displays given trip for editing purposes.
                 */
                FindParkingGoteborgApp.prototype.showTrip = function (trip) {
                    this._loginServ.getDestinations(trip);
                    //need promise this.newTrip = ...
                };
                /**
                 * Creates new destination if previously added one has filled adress field.
                 */
                FindParkingGoteborgApp.prototype.newDestination = function () {
                    var length = this.newTrip.destinations.length;
                    if (this.newTrip.destinations) {
                        if (this.newTrip.destinations.length == 0) {
                            this.newTrip.destinations.push(new destination_1.Destination());
                        }
                        else {
                            if (this.newTrip.destinations[length - 1].address) {
                                this.newTrip.destinations.push(new destination_1.Destination());
                            }
                        }
                        this.validateFormFields();
                    }
                };
                /**
                 * Creates new commentar for given destination object.
                 */
                FindParkingGoteborgApp.prototype.newComment = function (destination) {
                    var length = destination.comments.length;
                    if (destination.comments) {
                        if (destination.comments.length == 0) {
                            destination.comments.push(new comment_1.Comment());
                        }
                        else {
                            if (destination.comments[length - 1].text) {
                                destination.comments.push(new comment_1.Comment());
                            }
                        }
                        this.validateFormFields();
                    }
                };
                FindParkingGoteborgApp.prototype.ngOnInit = function () {
                    /*
                  this._loginServ.userAccount.subscribe(updatedTodos => {
                    
                  });
                  */
                    this.getLocal();
                    this.user = this._loginServ.getLocalUser();
                    this.user = this._loginServ.getLocalUser();
                    this.trips = this._loginServ.getTrips();
                    console.log('Test: ' + this.user + ' ' + this.trips.length);
                };
                /**
                 * When table row representing specific trip is clicked then this function shows
                 * trip for editing purposes.
                 */
                FindParkingGoteborgApp.prototype.tripClicked = function (trip) {
                    this.newTrip = trip;
                    this._loginServ.getDestinations(trip);
                };
                /**
                 * This method takes the values from the input fields,
                 * creates a trip object and a destination object and adds
                 * them to the database.
                 */
                FindParkingGoteborgApp.prototype.add = function () {
                    var _this = this;
                    var validated = this.validateFormFields();
                    if (validated === false) {
                        return;
                    }
                    var tripCreated = this._ref.child('/trips').push({
                        name: this.newTrip.name,
                        description: (this.newTrip.description) ? this.newTrip.description : null,
                        userid: this.user,
                        destinations: this.newTrip.destinations.length,
                        createdat: Firebase.ServerValue.TIMESTAMP
                    });
                    tripCreated.then(function () {
                        var _loop_1 = function(destination) {
                            _this.validateDestination(destination);
                            var destinationCreated = _this._ref.child('/destinations').push({
                                address: destination.address,
                                time: destination.time,
                                tripid: tripCreated.key(),
                                createdat: Firebase.ServerValue.TIMESTAMP
                            });
                            destinationCreated.then(function () {
                                for (var _i = 0, _a = destination.comments; _i < _a.length; _i++) {
                                    var comment = _a[_i];
                                    _this._ref.child('/comments').push({
                                        text: comment.text,
                                        destinationid: destinationCreated.key(),
                                        createdat: Firebase.ServerValue.TIMESTAMP
                                    });
                                }
                            });
                        };
                        for (var _i = 0, _a = _this.newTrip.destinations; _i < _a.length; _i++) {
                            var destination = _a[_i];
                            _loop_1(destination);
                        }
                        _this.newTrip = new trip_1.Trip();
                    });
                };
                FindParkingGoteborgApp.prototype.findParking = function (destination) {
                    var _this = this;
                    if (destination.address) {
                        var beforeAdress = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
                        var afterAdress = '&key=AIzaSyCoO_vczIXw8qrrNgaLzZNqNus5YFU9GnQ';
                        var address = destination.address;
                        if (destination.address.length > 7) {
                            if (destination.address.slice(-8) != "Göteborg") {
                                address += '+Göteborg';
                            }
                        }
                        else if (destination.address.length <= 7) {
                            address += '+Göteborg';
                        }
                        var object = this.apiService.getFromApi(beforeAdress + address + afterAdress).then(function (result) {
                            var geocoding = result.results[0].geometry.location;
                            _this.getParking(geocoding, destination, 100);
                        });
                    }
                };
                /**
                 * Function removeLastDestination removes last destination from list of destinations
                 * to newly created trip object. If trip has no destinations, none are removed.
                 */
                FindParkingGoteborgApp.prototype.removeLastDestination = function () {
                    if (this.newTrip.destinations.length > 0) {
                        this.newTrip.destinations.pop();
                    }
                };
                /**
                 * Function removeLastComment takes destination as argument and removes last Comment
                 * object that is assigned to its variable of comments if any.
                 */
                FindParkingGoteborgApp.prototype.removeLastComment = function (destination) {
                    if (destination && destination.comments.length > 0) {
                        destination.comments.pop();
                    }
                };
                /**
                 * Function goes through object of Destination class and checks if it has any valid values
                 * on some of its variables. Some get default value if they have none for the moment.
                 */
                FindParkingGoteborgApp.prototype.validateDestination = function (destination) {
                    if (!destination.address) {
                        destination.address = 'none';
                    }
                    if (!destination.comments) {
                    }
                    if (!destination.time) {
                        destination.time = '0';
                    }
                };
                /**
                 * Checks if trip object has name assigned to it, if not then it assigns name to it.
                 */
                FindParkingGoteborgApp.prototype.validateTripName = function (trip) {
                    if (!trip.name) {
                        trip.name = 'No name';
                    }
                };
                /**
                 * Function validateFormFields goes through form fields with id 'name' or class 'adress-field'
                 * and checks if they have input value. If any of these fields is empty then this function returns
                 * false aswell as it marks fields with class representing that they have invalid input. If field
                 * became valid and has invalid class then its removed.
                 */
                FindParkingGoteborgApp.prototype.validateFormFields = function () {
                    var isValid = true;
                    var tripElement = document.getElementById('name');
                    var tripName = tripElement.value;
                    var adressElements = document.getElementsByClassName('adress-field');
                    var commentElements = document.getElementsByClassName('comment-field');
                    //check if name is valid
                    if (!tripName || tripName.length > 30) {
                        tripElement.className += ' field-input-invalid'; //mark element as invalid
                        isValid = false;
                    }
                    else {
                        tripElement.classList.remove('field-input-invalid');
                    }
                    //check if destination adresses are valid
                    if (adressElements) {
                        for (var i = 0; i < adressElements.length; i++) {
                            if (!adressElements[i].value) {
                                //mark element with invalid class ng-invalid
                                adressElements[i].classList.add('field-input-invalid');
                                isValid = false;
                            }
                            else {
                                adressElements[i].classList.remove('field-input-invalid');
                            }
                        }
                    }
                    //check if destination comments are valid
                    if (commentElements) {
                        for (var i = 0; i < commentElements.length; i++) {
                            if (!commentElements[i].value) {
                                //mark element with invalid class ng-invalid
                                commentElements[i].classList.add('field-input-invalid');
                                isValid = false;
                            }
                            else {
                                commentElements[i].classList.remove('field-input-invalid');
                            }
                        }
                    }
                    return isValid;
                };
                FindParkingGoteborgApp.prototype.startNewTrip = function () {
                    this.newTrip = new trip_1.Trip();
                };
                /*Account management */
                FindParkingGoteborgApp.prototype.getParking = function (position, destination, radius) {
                    var _this = this;
                    var parkings = [];
                    this.parkingService.publicTimeParkings(position, destination, radius, parkings).then(function (freeList) {
                        _this.parkingService.publicTollParkings(position, destination, radius, freeList).then(function (completeList) {
                            if (completeList.length < _this.minNumberOfParkings) {
                                _this.getParking(position, destination, radius + 50);
                            }
                            else {
                                completeList.sort(function (a, b) {
                                    return a.distance - b.distance;
                                });
                                destination.parking = completeList;
                            }
                        });
                    });
                };
                // TEST
                FindParkingGoteborgApp.prototype.getLocal = function () {
                    var _this = this;
                    this._loginServ.getLocalUserP().then(function (userKey) {
                        _this.user = userKey;
                        _this.user = _this._loginServ.getLocalUser();
                        _this.trips = _this._loginServ.getTrips();
                        console.log('Test: ' + _this.user + ' ' + _this.trips.length);
                        console.log(_this.user + 'bbb');
                    });
                };
                FindParkingGoteborgApp = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'find-parking-goteborg-app',
                        providers: [api_service_1.ApiService, parking_service_1.ParkingService],
                        templateUrl: 'find-parking-goteborg.component.html',
                        styleUrls: ['find-parking-goteborg.component.css'],
                        directives: [],
                        pipes: []
                    }),
                    __param(0, core_1.Inject(angularfire2_1.FirebaseRef)), 
                    __metadata('design:paramtypes', [Firebase, api_service_1.ApiService, parking_service_1.ParkingService, account_service_1.AccountService])
                ], FindParkingGoteborgApp);
                return FindParkingGoteborgApp;
            }());
            exports_1("FindParkingGoteborgApp", FindParkingGoteborgApp);
        }
    }
});
//# sourceMappingURL=find-parking-goteborg.component.js.map