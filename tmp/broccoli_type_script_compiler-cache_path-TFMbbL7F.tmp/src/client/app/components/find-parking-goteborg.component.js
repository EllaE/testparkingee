System.register(['angular2/core', 'angularfire2', '../supporting-files/destination', '../supporting-files/trip', '../supporting-files/comment', '../services/api.service', '../supporting-files/user', '../services/parking.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, angularfire2_1, destination_1, trip_1, comment_1, api_service_1, user_1, parking_service_1;
    var FindParkingGoteborgApp;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (angularfire2_1_1) {
                angularfire2_1 = angularfire2_1_1;
            },
            function (destination_1_1) {
                destination_1 = destination_1_1;
            },
            function (trip_1_1) {
                trip_1 = trip_1_1;
            },
            function (comment_1_1) {
                comment_1 = comment_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (user_1_1) {
                user_1 = user_1_1;
            },
            function (parking_service_1_1) {
                parking_service_1 = parking_service_1_1;
            }],
        execute: function() {
            FindParkingGoteborgApp = (function () {
                function FindParkingGoteborgApp(_ref, apiService, parkingService) {
                    this._ref = _ref;
                    this.apiService = apiService;
                    this.parkingService = parkingService;
                    this.minNumberOfParkings = 3;
                    this.trips = [];
                    this.newTrip = new trip_1.Trip();
                }
                /**
                 * getTrip fetches all trip data for specific user from firebase.
                 */
                FindParkingGoteborgApp.prototype.getTrip = function () {
                    var _this = this;
                    this._ref.child('/trips').orderByChild('userid').equalTo(this.user).on('value', function (userTrips) {
                        _this.trips = [];
                        userTrips.forEach(function (trip) {
                            var t = new trip_1.Trip();
                            t.name = trip.child("name").val();
                            t.description = trip.child('description').val();
                            t.id = trip.key();
                            _this.trips.push(t);
                        });
                        _this.trips.reverse();
                    });
                };
                /**
                 * Fetches destinations for given Trip object from firebase.
                 */
                FindParkingGoteborgApp.prototype.getDestinations = function (trip) {
                    var _this = this;
                    trip.destinations = [];
                    this._ref.child('/destinations').orderByChild('tripid').equalTo(trip.id).on('value', function (tripDestinations) {
                        tripDestinations.forEach(function (destination) {
                            var d = new destination_1.Destination();
                            d.id = destination.key();
                            d.address = destination.child('address').val();
                            d.time = destination.child('time').val();
                            trip.destinations.push(d);
                            _this.getComments(d);
                        });
                    });
                };
                /**
                 * Fetches comments for given destination object from firebase database.
                 */
                FindParkingGoteborgApp.prototype.getComments = function (destination) {
                    destination.comments = [];
                    this._ref.child('/comments').orderByChild('destinationid').equalTo(destination.id).on('value', function (destinationComments) {
                        destinationComments.forEach(function (comment) {
                            var c = new comment_1.Comment();
                            c.text = comment.child('text').val();
                            c.timestamp = comment.child('createdat').val();
                            destination.comments.push(c);
                        });
                    });
                };
                /**
                 * showTrip displays given trip for editing purposes.
                 */
                FindParkingGoteborgApp.prototype.showTrip = function (trip) {
                    this.getDestinations(trip);
                };
                /**
                 * Creates new destination if previously added one has filled adress field.
                 */
                FindParkingGoteborgApp.prototype.newDestination = function () {
                    var length = this.newTrip.destinations.length;
                    if (this.newTrip.destinations) {
                        if (this.newTrip.destinations.length == 0) {
                            this.newTrip.destinations.push(new destination_1.Destination());
                        }
                        else {
                            if (this.newTrip.destinations[length - 1].address) {
                                this.newTrip.destinations.push(new destination_1.Destination());
                            }
                        }
                        this.validateFormFields();
                    }
                };
                /**
                 * Creates new commentar for given destination object.
                 */
                FindParkingGoteborgApp.prototype.newComment = function (destination) {
                    var length = destination.comments.length;
                    if (destination.comments) {
                        if (destination.comments.length == 0) {
                            destination.comments.push(new comment_1.Comment());
                        }
                        else {
                            if (destination.comments[length - 1].text) {
                                destination.comments.push(new comment_1.Comment());
                            }
                        }
                        this.validateFormFields();
                    }
                };
                FindParkingGoteborgApp.prototype.ngOnInit = function () {
                    this.loadUser();
                    this.handleDefaultAccountDOM();
                };
                /**
                 * When table row representing specific trip is clicked then this function shows
                 * trip for editing purposes.
                 */
                FindParkingGoteborgApp.prototype.tripClicked = function (trip) {
                    this.newTrip = trip;
                    this.getDestinations(trip);
                };
                /**
                 * This method takes the values from the input fields,
                 * creates a trip object and a destination object and adds
                 * them to the database.
                 */
                FindParkingGoteborgApp.prototype.add = function () {
                    var _this = this;
                    var validated = this.validateFormFields();
                    if (validated === false) {
                        return;
                    }
                    var tripCreated = this._ref.child('/trips').push({
                        name: this.newTrip.name,
                        description: (this.newTrip.description) ? this.newTrip.description : null,
                        userid: this.user,
                        destinations: this.newTrip.destinations.length,
                        createdat: Firebase.ServerValue.TIMESTAMP
                    });
                    tripCreated.then(function () {
                        var _loop_1 = function(destination) {
                            _this.validateDestination(destination);
                            var destinationCreated = _this._ref.child('/destinations').push({
                                address: destination.address,
                                time: destination.time,
                                tripid: tripCreated.key(),
                                createdat: Firebase.ServerValue.TIMESTAMP
                            });
                            destinationCreated.then(function () {
                                for (var _i = 0, _a = destination.comments; _i < _a.length; _i++) {
                                    var comment = _a[_i];
                                    _this._ref.child('/comments').push({
                                        text: comment.text,
                                        destinationid: destinationCreated.key(),
                                        createdat: Firebase.ServerValue.TIMESTAMP
                                    });
                                }
                            });
                        };
                        for (var _i = 0, _a = _this.newTrip.destinations; _i < _a.length; _i++) {
                            var destination = _a[_i];
                            _loop_1(destination);
                        }
                        _this.newTrip = new trip_1.Trip();
                    });
                };
                FindParkingGoteborgApp.prototype.findParking = function (destination) {
                    var _this = this;
                    if (destination.address) {
                        var beforeAdress = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
                        var afterAdress = '&key=AIzaSyCoO_vczIXw8qrrNgaLzZNqNus5YFU9GnQ';
                        var address = destination.address;
                        if (destination.address.length > 7) {
                            if (destination.address.slice(-8) != "Göteborg") {
                                address += '+Göteborg';
                            }
                        }
                        else if (destination.address.length <= 7) {
                            address += '+Göteborg';
                        }
                        var object = this.apiService.getFromApi(beforeAdress + address + afterAdress).then(function (result) {
                            var geocoding = result.results[0].geometry.location;
                            _this.getParking(geocoding, destination, 100);
                        });
                    }
                };
                /**  This method creates new user token for
                 * firebase and saves it in local storage
                 */
                FindParkingGoteborgApp.prototype.createAndSaveUser = function () {
                    var _this = this;
                    localStorage.clear();
                    var userCreated = this._ref.child('/users').push({
                        timestamp: Firebase.ServerValue.TIMESTAMP
                    });
                    userCreated.then(function (data) {
                        _this.user = userCreated.key();
                        localStorage.setItem('USER_DB', _this.user);
                        _this.getTrip();
                    });
                };
                /** This method loads user token from local storage
                 * if any existent, otherwise it runs createAndSaveUser
                 */
                FindParkingGoteborgApp.prototype.loadUser = function () {
                    var _this = this;
                    if (localStorage.getItem('USER_DB')) {
                        this.user = localStorage.getItem('USER_DB');
                        this._ref.child('/users/' + this.user).once('value', function (result) {
                            if (result.exists()) {
                                _this.getTrip();
                            }
                            else {
                                _this.createAndSaveUser();
                            }
                        });
                    }
                    else {
                        this.createAndSaveUser();
                    }
                };
                /**
                 * Function removeLastDestination removes last destination from list of destinations
                 * to newly created trip object. If trip has no destinations, none are removed.
                 */
                FindParkingGoteborgApp.prototype.removeLastDestination = function () {
                    if (this.newTrip.destinations.length > 0) {
                        this.newTrip.destinations.pop();
                    }
                };
                /**
                 * Function removeLastComment takes destination as argument and removes last Comment
                 * object that is assigned to its variable of comments if any.
                 */
                FindParkingGoteborgApp.prototype.removeLastComment = function (destination) {
                    if (destination && destination.comments.length > 0) {
                        destination.comments.pop();
                    }
                };
                /**
                 * Function goes through object of Destination class and checks if it has any valid values
                 * on some of its variables. Some get default value if they have none for the moment.
                 */
                FindParkingGoteborgApp.prototype.validateDestination = function (destination) {
                    if (!destination.address) {
                        destination.address = 'none';
                    }
                    if (!destination.comments) {
                    }
                    if (!destination.time) {
                        destination.time = '0';
                    }
                };
                /**
                 * Checks if trip object has name assigned to it, if not then it assigns name to it.
                 */
                FindParkingGoteborgApp.prototype.validateTripName = function (trip) {
                    if (!trip.name) {
                        trip.name = 'No name';
                    }
                };
                /**
                 * Function validateFormFields goes through form fields with id 'name' or class 'adress-field'
                 * and checks if they have input value. If any of these fields is empty then this function returns
                 * false aswell as it marks fields with class representing that they have invalid input. If field
                 * became valid and has invalid class then its removed.
                 */
                FindParkingGoteborgApp.prototype.validateFormFields = function () {
                    var isValid = true;
                    var tripElement = document.getElementById('name');
                    var tripName = tripElement.value;
                    var adressElements = document.getElementsByClassName('adress-field');
                    var commentElements = document.getElementsByClassName('comment-field');
                    //check if name is valid
                    if (!tripName || tripName.length > 30) {
                        tripElement.className += ' field-input-invalid'; //mark element as invalid
                        isValid = false;
                    }
                    else {
                        tripElement.classList.remove('field-input-invalid');
                    }
                    //check if destination adresses are valid
                    if (adressElements) {
                        for (var i = 0; i < adressElements.length; i++) {
                            if (!adressElements[i].value) {
                                //mark element with invalid class ng-invalid
                                adressElements[i].classList.add('field-input-invalid');
                                isValid = false;
                            }
                            else {
                                adressElements[i].classList.remove('field-input-invalid');
                            }
                        }
                    }
                    //check if destination comments are valid
                    if (commentElements) {
                        for (var i = 0; i < commentElements.length; i++) {
                            if (!commentElements[i].value) {
                                //mark element with invalid class ng-invalid
                                commentElements[i].classList.add('field-input-invalid');
                                isValid = false;
                            }
                            else {
                                commentElements[i].classList.remove('field-input-invalid');
                            }
                        }
                    }
                    return isValid;
                };
                FindParkingGoteborgApp.prototype.startNewTrip = function () {
                    this.newTrip = new trip_1.Trip();
                };
                /*Account management */
                /**
                 * logOutUser is trigged when logged in user clicks on logout button and
                 * then user is logged out from page and localstorage user is used or made
                 * if none existent.
                 */
                FindParkingGoteborgApp.prototype.logOutUser = function () {
                    //1. remove all user reference, check for loclalStorage if any
                    this.handleDefaultAccountDOM();
                    this.user = null;
                    this.trips = [];
                    this.newTrip = new trip_1.Trip();
                    this.loadUser(); //load local storage user if any, create new otherwise
                };
                /**
                 * logInUser takes User object as input, then it checks in database for users
                 * with given name, loads user if any and if password matches.
                 */
                FindParkingGoteborgApp.prototype.logInUser = function (user) {
                    var _this = this;
                    if (!user.name || !user.logInPassword) {
                        this.displayAccountFormFeedback('Fill name and password');
                        return;
                    }
                    this._ref.child('/userAccounts').orderByChild('name').equalTo(user.name).on('value', function (userAccounts) {
                        userAccounts.forEach(function (aUser) {
                            if (user.logInPassword === aUser.child('password').val()) {
                                user.setPassword(aUser.child('name').val());
                                user.userKey = aUser.key(); //needed to load trips
                            }
                        });
                        if (user.userKey) {
                            _this.user = user.userKey;
                            _this.getTrip(); //load user trips 
                            _this.newTrip = new trip_1.Trip(); //otherwise can be some weird probs when editing trips?
                            _this.defaultLoggedInAccountDOM();
                        }
                        else {
                            _this.displayAccountFormFeedback('Incorrect name or password');
                        }
                    });
                };
                /**
                 * CreateUser creates new user account, saves it on firebase database and then logs in user.
                 */
                FindParkingGoteborgApp.prototype.createUser = function (user) {
                    var _this = this;
                    if (user.name && user.logInPassword) {
                        this._ref.child('/userAccounts').orderByChild('name').equalTo(user.name).once('value', function (databaseUsers) {
                            if (!databaseUsers.exists()) {
                                var userCreated = _this._ref.child('/userAccounts').push({
                                    name: user.name,
                                    password: user.logInPassword
                                });
                                userCreated.then(function () {
                                    //log in user!
                                    _this.logInUser(user);
                                });
                            }
                            else {
                                _this.displayAccountFormFeedback('Username already exist!');
                            }
                        });
                    }
                    else {
                        this.displayAccountFormFeedback('Fill name and password');
                    }
                };
                /*Account management - DOM  */
                /**
                 * Takes string text to be shown as feedback when user interacts with user creation/login
                 * form. Displays text on that form.
                 */
                FindParkingGoteborgApp.prototype.displayAccountFormFeedback = function (feedbackText) {
                    var feedbackSpan = document.getElementById('feedback-account-form');
                    feedbackSpan.innerText = '';
                    feedbackSpan.innerText = feedbackText;
                };
                /**
                 * Removes input from input fields username and password in user creation/log in form.
                 */
                FindParkingGoteborgApp.prototype.cleanAccountForm = function () {
                    this.displayAccountFormFeedback(' ');
                    document.getElementById('username').value = '';
                    document.getElementById('password').value = '';
                };
                /**
                 * CreateOrLogIn is called when account form submit button is clicked. Depending on value on
                 * that button (inner text) this function will either start function to log in user or to
                 * create new account.
                 */
                FindParkingGoteborgApp.prototype.createOrLogIn = function () {
                    if (document.getElementById('confirm-btn-account').innerText === 'Login') {
                        this.logInUser(this.userAccount);
                    }
                    else {
                        this.createUser(this.userAccount);
                    }
                };
                /**
                 * Shows or hides log in row depending if user is logged in or not.
                 */
                FindParkingGoteborgApp.prototype.showRegisterOrUser = function () {
                    this.cleanAccountForm();
                    if (this.userAccount.userKey) {
                        document.getElementById('loggedout-form').style.display = 'none';
                        document.getElementById('loggedin-form').style.display = 'block';
                    }
                    else {
                        document.getElementById('loggedin-form').style.display = 'none';
                        document.getElementById('loggedout-form').style.display = 'block';
                    }
                };
                /**
                 * Shows or hides account registration/ account log in form depending on if register/login button
                 * was clicked and if it was clicked again or not.
                 */
                FindParkingGoteborgApp.prototype.showHideAccountForm = function (type) {
                    var accountForm = document.getElementById('account-form');
                    var confirmBtn = document.getElementById('confirm-btn-account');
                    if (!(accountForm) || accountForm.style.display === 'none') {
                        accountForm.style.display = 'block';
                        confirmBtn.innerText = type;
                        this.cleanAccountForm();
                    }
                    else {
                        if (type !== confirmBtn.innerText) {
                            confirmBtn.innerText = type;
                            this.cleanAccountForm();
                        }
                        else {
                            accountForm.style.display = 'none';
                        }
                    }
                };
                /**
                 * This function is to be called when webpage application is loaded for first time. It sets up
                 * default look on webpage which is no user logged in.
                 */
                FindParkingGoteborgApp.prototype.handleDefaultAccountDOM = function () {
                    this.userAccount = new user_1.User();
                    this.showRegisterOrUser();
                    document.getElementById('account-form').style.display = 'none';
                };
                /**
                 * Handles display of user when logged in.
                 */
                FindParkingGoteborgApp.prototype.defaultLoggedInAccountDOM = function () {
                    this.showRegisterOrUser();
                    document.getElementById('account-form').style.display = 'none';
                };
                /**
                 * Called when user clicks on Login link on webpage.
                 */
                FindParkingGoteborgApp.prototype.logInForm = function () {
                    //show login-component
                    this.showHideAccountForm('Login');
                };
                /**
                 * Called when user clicks on Register link on webpage.
                 */
                FindParkingGoteborgApp.prototype.registerNewUserForm = function () {
                    //show new-account-component 
                    this.showHideAccountForm('Register');
                };
                FindParkingGoteborgApp.prototype.getParking = function (position, destination, radius) {
                    var _this = this;
                    var parkings = [];
                    this.parkingService.publicTimeParkings(position, destination, radius, parkings).then(function (freeList) {
                        _this.parkingService.publicTollParkings(position, destination, radius, freeList).then(function (completeList) {
                            if (completeList.length < _this.minNumberOfParkings) {
                                _this.getParking(position, destination, radius + 50);
                            }
                            else {
                                completeList.sort(function (a, b) {
                                    return a.distance - b.distance;
                                });
                                destination.parking = completeList;
                            }
                        });
                    });
                };
                FindParkingGoteborgApp = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'find-parking-goteborg-app',
                        providers: [api_service_1.ApiService, parking_service_1.ParkingService],
                        templateUrl: 'find-parking-goteborg.component.html',
                        styleUrls: ['find-parking-goteborg.component.css'],
                        directives: [],
                        pipes: []
                    }),
                    __param(0, core_1.Inject(angularfire2_1.FirebaseRef)), 
                    __metadata('design:paramtypes', [Firebase, api_service_1.ApiService, parking_service_1.ParkingService])
                ], FindParkingGoteborgApp);
                return FindParkingGoteborgApp;
            }());
            exports_1("FindParkingGoteborgApp", FindParkingGoteborgApp);
        }
    }
});
//# sourceMappingURL=find-parking-goteborg.component.js.map