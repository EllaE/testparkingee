System.register(['angular2/core', 'angular2/router', '../services/api.service', '../services/parking.service', '../components/parking-history.component', '../components/find-parking-goteborg.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, api_service_1, parking_service_1, parking_history_component_1, find_parking_goteborg_component_1, router_2;
    var MenyGoteborgApp;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (parking_service_1_1) {
                parking_service_1 = parking_service_1_1;
            },
            function (parking_history_component_1_1) {
                parking_history_component_1 = parking_history_component_1_1;
            },
            function (find_parking_goteborg_component_1_1) {
                find_parking_goteborg_component_1 = find_parking_goteborg_component_1_1;
            }],
        execute: function() {
            MenyGoteborgApp = (function () {
                function MenyGoteborgApp(router) {
                    this.router = router;
                }
                MenyGoteborgApp.prototype.ngOnInit = function () {
                    // this.router.navigate(['/history']);
                };
                MenyGoteborgApp = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'meny-goteborg-app',
                        providers: [api_service_1.ApiService, parking_service_1.ParkingService, router_1.ROUTER_PROVIDERS],
                        templateUrl: 'meny-goteborg.component.html',
                        styleUrls: ['meny-goteborg.component.css'],
                        directives: [router_1.ROUTER_DIRECTIVES, router_2.RouterOutlet, router_2.RouterLink, parking_history_component_1.ParkingHistory, find_parking_goteborg_component_1.FindParkingGoteborgApp],
                        pipes: []
                    }),
                    router_1.RouteConfig([
                        { path: '/find-parking', component: find_parking_goteborg_component_1.FindParkingGoteborgApp, name: 'Home' },
                        { path: '/history', component: parking_history_component_1.ParkingHistory, name: 'History' },
                        { path: '/**', redirectTo: ['Home'] }
                    ]), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], MenyGoteborgApp);
                return MenyGoteborgApp;
            }());
            exports_1("MenyGoteborgApp", MenyGoteborgApp);
        }
    }
});
//# sourceMappingURL=meny-goteborg.component.js.map