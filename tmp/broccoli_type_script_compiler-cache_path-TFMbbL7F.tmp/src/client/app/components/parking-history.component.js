System.register(['angular2/core', '../services/api.service', '../services/parking.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, api_service_1, parking_service_1;
    var ParkingHistory;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (api_service_1_1) {
                api_service_1 = api_service_1_1;
            },
            function (parking_service_1_1) {
                parking_service_1 = parking_service_1_1;
            }],
        execute: function() {
            ParkingHistory = (function () {
                function ParkingHistory() {
                }
                ParkingHistory = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'parking-history',
                        providers: [api_service_1.ApiService, parking_service_1.ParkingService],
                        templateUrl: 'parking-history.component.html',
                        styleUrls: ['parking-history.component.css'],
                        directives: [],
                        pipes: []
                    }), 
                    __metadata('design:paramtypes', [])
                ], ParkingHistory);
                return ParkingHistory;
            }());
            exports_1("ParkingHistory", ParkingHistory);
        }
    }
});
//# sourceMappingURL=parking-history.component.js.map