System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Trip;
    return {
        setters:[],
        execute: function() {
            /**
             * Class Trip represents trip that user has planned along with its description,
             * destination (parking) adresses with comments and time for every destination.
             */
            Trip = (function () {
                function Trip() {
                    this.destinations = [];
                }
                return Trip;
            }());
            exports_1("Trip", Trip);
        }
    }
});
//# sourceMappingURL=trip.js.map