System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Comment;
    return {
        setters:[],
        execute: function() {
            /**
             * Class Comment is used to save information about comment made for a specific
             * destination and timestamp when that comment was created.
             */
            Comment = (function () {
                function Comment() {
                }
                return Comment;
            }());
            exports_1("Comment", Comment);
        }
    }
});
//# sourceMappingURL=comment.js.map