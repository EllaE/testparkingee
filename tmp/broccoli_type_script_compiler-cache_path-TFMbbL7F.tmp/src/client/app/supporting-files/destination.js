System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Destination;
    return {
        setters:[],
        execute: function() {
            /**
             * Class Destination represents one stop, adress where user plans to spend
             * given time. One Destination has adress, none or many comments, parkings
             * nearby and time information.
             */
            Destination = (function () {
                function Destination() {
                    this.parking = new Array();
                    this.comments = [];
                }
                return Destination;
            }());
            exports_1("Destination", Destination);
        }
    }
});
//# sourceMappingURL=destination.js.map