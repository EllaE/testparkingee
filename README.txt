To install and run project

In terminal:
sudo npm install
sudo typings install

To run project:
sudo ng serve 
(if port taken sudo ng serve --port port-number)
in browser enter localhost:port-number (default 4200)

If errors occur:
Typings not installed
sudo npm install typings --global (or sudo npm install -g typings)

When serving cant find assets
sudo ng build

If none of these work refer to https://github.com/angular/angular-cli/issues for more information
